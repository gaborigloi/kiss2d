//! Easy drawing functions for glium.
//!
//! Provides a lightweight struct that allows one to easily draw lines
//! onto a glium::Surface.

#[macro_use]
extern crate glium;

use std::error::Error;

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
}

implement_vertex!(Vertex, position);

pub const VERTICES_PER_LINE: usize = 2;
pub const VERTICES_PER_ARROW: usize = 6;

/// A batch of lines that can be drawn to a glium::Surface.
///
/// Contains the structs necessary for drawing the lines.
pub struct Canvas {
    vertex_buffer: glium::VertexBuffer<Vertex>,
    program: glium::Program,
    viewport_transform: [[f32; 4]; 4],
    vertex_list: Vec<Vertex>
}

impl Canvas {

    /// Creates a new Canvas with a custom viewport.
    ///
    /// * compiles the shader program
    /// * creates the vertex buffer used by the Canvas, with a fixed maximum capacity `max_vertex_count`
    ///
    /// The arguments bottom_left and top_right specify the [x,y] coordinates of the
    /// bottom-left and top-right corners of the target.
    pub fn from_viewport<F>(facade: &F,
                            max_vertex_count: usize,
                            bottom_left: [f32; 2],
                            top_right: [f32; 2]) -> Result<Self, Box<Error>>
        where F: glium::backend::Facade {
        // map the required viewport to the OpenGL device coordinates: bottom-left (-1, -1), top-right (1, 1)
        assert!(bottom_left[0] != top_right[0], "the width of the viewport shouldn't be 0");
        assert!(bottom_left[1] != top_right[1], "the height of the viewport shouldn't be 0");
        let scale_x = 2.0 / (top_right[0] - bottom_left[0]);
        let scale_y = 2.0 / (top_right[1] - bottom_left[1]);
        let add_x = - (top_right[0] + bottom_left[0]) / (top_right[0] - bottom_left[0]);
        let add_y = - (top_right[1] + bottom_left[1]) / (top_right[1] - bottom_left[1]);
        // OpenGL uses column-major format:
        let matrix = [
            [scale_x, 0.0,     0.0, 0.0],
            [0.0,     scale_y, 0.0, 0.0],
            [0.0,     0.0,     1.0, 0.0],
            [add_x,   add_y,   0.0, 1.0_f32],
        ];

        let program = {
            let vertex_shader_src = r#"
                #version 140

                in vec2 position;

                uniform mat4 matrix;

                void main() {
                    gl_Position = matrix * vec4(position, 0.0, 1.0);
                }
            "#;

            let fragment_shader_src = r#"
                #version 140

                out vec4 color;

                void main() {
                    color = vec4(1.0, 0.0, 0.0, 1.0);
                }
            "#;

            try!(glium::Program::from_source(facade, vertex_shader_src, fragment_shader_src, None))
        };
        Ok(Canvas {
            vertex_buffer: try!(glium::VertexBuffer::empty_dynamic(facade, max_vertex_count)),
            program: program,
            viewport_transform: matrix,
            vertex_list: Vec::with_capacity(max_vertex_count)
        })
    }

    /// Creates a new Canvas.
    ///
    /// * compiles the shader program
    /// * creates the vertex buffer used by the Canvas, with a fixed maximum capacity `max_vertex_count`
    ///
    /// The default OpenGL viewport will be used for line coordinates:
    ///
    /// * bottom-left (-1, -1),
    /// * top-right (1, 1)
    pub fn new<F>(facade: &F, max_vertex_count: usize) -> Result<Self, Box<Error>>
        where F: glium::backend::Facade {
        Self::from_viewport(facade, max_vertex_count, [-1.0, -1.0], [1.0, 1.0])
    }

    /// Add a line to the Canvas between points a and b.
    pub fn add_line(&mut self, a: [f32; 2], b: [f32; 2]) {
        self.vertex_list.push(Vertex { position: a });
        self.vertex_list.push(Vertex { position: b });
    }

    pub fn add_arrow(&mut self, from: [f32; 2], to: [f32; 2], arrow_size: f32) {
        use std::f32::consts::PI;

        self.add_line(from, to);
        let arrow_size = {
            let length = ((to[0] - from[0]).powi(2) + (to[1] - from[1]).powi(2)).sqrt();
            arrow_size.min(length)
        };
        let angle = (to[1] - from[1]).atan2(to[0] - from[0]);
        let to_angle = PI + angle;
        let arrow_angle = 15.0_f32.to_radians();
        let head_angles = ((to_angle + arrow_angle), (to_angle - arrow_angle));
        self.add_line(to,
                      [to[0] + arrow_size * head_angles.0.cos(),
                       to[1] + arrow_size * head_angles.0.sin()]);
        self.add_line(to,
                      [to[0] + arrow_size * head_angles.1.cos(),
                       to[1] + arrow_size * head_angles.1.sin()]);
    }

    pub fn draw<S: glium::Surface>(&self, surface: &mut S) -> Result<(),Box<Error>>  {
        //TODO should return a custom error type instead so users will know the capacity was exceeded
        //TODO since we can only add lines, we don't have to refill the whole buffer, just write the new
        // ones to it - we should have a queue of newly added vertices and remove and upload them here:
        let slice = try!(
            self.vertex_buffer.slice(0..self.vertex_list.len()).ok_or("exceeded max_vertex_count")
        );
        slice.write(&self.vertex_list);
        let indices = glium::index::NoIndices(glium::index::PrimitiveType::LinesList);
        let uniforms = uniform! {
            matrix: self.viewport_transform
        };
        try!(surface.draw(&self.vertex_buffer,
                          &indices,
                          &self.program,
                          &uniforms,
                          &Default::default()));
        Ok(())
    }

    /// Removes all lines from the canvas.
    //FIXME remove - this is redundant, we can just create a new canvas
    pub fn clear(&mut self) {
        self.vertex_list.clear();
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
