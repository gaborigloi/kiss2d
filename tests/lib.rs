extern crate kiss2d;

#[macro_use]
extern crate glium;
#[macro_use]
extern crate timeit;

use kiss2d::*;
use glium::{DisplayBuild, Surface};

use std::thread;
use std::time::Duration;

fn display_canvas(display: &glium::Display, canvas: &Canvas) {
    use glium::glutin::Event::KeyboardInput;
    use glium::glutin::{VirtualKeyCode, ElementState};

    loop {
        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Space))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Escape))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Return))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::NumpadEnter)) => return,
                glium::glutin::Event::Refresh => {
                    let mut target = display.draw();
                    target.clear_color(0.0, 0.0, 1.0, 1.0);
                    canvas.draw(&mut target).unwrap();
                    target.finish().unwrap();
                },
                _ => ()
            }
        }
        thread::sleep(Duration::from_millis(100));
    }
}

// Line drawing tests
// The following 4 tests should show *exactly* the same "triangle" (only two sides).

#[test]
// ignore visual tests
#[ignore]
fn display_drawn_lines() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::new(&display, 2 * VERTICES_PER_LINE).unwrap();

    canvas.add_line([-0.5, -0.5], [0.0, 0.5]);
    canvas.add_line([ 0.0, 0.5], [0.5, -0.25]);

    display_canvas(&display, &canvas);
}

#[test]
#[ignore]
fn same_opengl_viewport() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::from_viewport(&display, 20, [-1.0, -1.0], [1.0, 1.0]).unwrap();

    canvas.add_line([-0.5, -0.5], [0.0, 0.5]);
    canvas.add_line([ 0.0, 0.5], [0.5, -0.25]);

    display_canvas(&display, &canvas);
}

#[test]
#[ignore]
fn custom_viewport() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::from_viewport(&display, 20, [0.0, 0.0], [300.0, 300.0]).unwrap();

    canvas.add_line([75.0, 75.0], [150.0, 225.0]);
    canvas.add_line([150.0, 225.0], [225.0, 112.5]);

    display_canvas(&display, &canvas);
}

#[test]
#[ignore]
fn computer_graphics_viewport_flipped_y() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::from_viewport(&display, 20, [0.0, 300.0], [300.0, 0.0]).unwrap();

    canvas.add_line([75.0, 225.0], [150.0, 75.0]);
    canvas.add_line([150.0, 75.0], [225.0, 187.5]);

    display_canvas(&display, &canvas);
}

// Arrow drawing tests
// The following 2 tests should show *exactly* the same arrows.

#[test]
#[ignore]
fn display_drawn_arrows_default_viewport() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::new(&display, 2 * VERTICES_PER_ARROW).unwrap();

    canvas.add_arrow([-0.5, -0.5], [0.0, 0.5], 0.2);
    canvas.add_arrow([ 0.0, 0.5], [0.5, -0.25], 0.2);

    display_canvas(&display, &canvas);
}

#[test]
#[ignore]
fn display_drawn_arrows_custom_viewport() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::from_viewport(&display, 20, [0.0, 0.0], [300.0, 300.0]).unwrap();

    canvas.add_arrow([75.0, 75.0], [150.0, 225.0], 30.0);
    canvas.add_arrow([150.0, 225.0], [225.0, 112.5], 30.0);

    display_canvas(&display, &canvas);
}

#[test]
#[ignore]
fn draw_small_arrows() {
    let display = glium::glutin::WindowBuilder::new().with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::from_viewport(&display, 30, [-150.0, -150.0], [150.0, 150.0]).unwrap();

    canvas.add_arrow([0.0, 0.0], [0.0, -5.0], 30.0);
    canvas.add_arrow([0.0, 0.0], [-10.0, 0.0], 30.0);
    canvas.add_arrow([0.0, 0.0], [0.0, 20.0], 30.0);
    canvas.add_arrow([0.0, 0.0], [40.0, 0.0], 30.0);

    display_canvas(&display, &canvas);
}

#[test]
#[ignore]
fn time_performance() {
    let display = glium::glutin::WindowBuilder::new().with_visibility(false).with_dimensions(300, 300).build_glium().unwrap();

    let mut canvas = Canvas::new(&display, 20_000).unwrap();

    timeit!({
        let mut target = display.draw();
        canvas.clear();
        target.clear_color(1.0, 1.0, 1.0, 1.0);
        for _ in 0..130 {
            for _ in 0..70 {
                canvas.add_line([0.0, 0.0], [100.0, 100.0]);
            }
        }
        canvas.draw(&mut target).unwrap();
        target.finish().unwrap();
    });
}

#[test]
#[ignore]
/// FIXME possible to draw to wrong display:
///
/// This test demonstrates that currently it is possible to draw to
/// a different display, that doesn't share the context of the display
/// ofr which the Canvas was created, containing its vertex buffer.
///
/// Specifying the Display we draw on in the constructor is
/// not flexible since we may want to draw the same thing to the screen
/// and then to a SrgbTexture2d, for example.
fn lifetime_test() {
    let display1 = glium::glutin::WindowBuilder::new()
        .with_dimensions(300, 300)
        .with_title("display1".to_owned())
        .build_glium().unwrap();
    let display2 = glium::glutin::WindowBuilder::new()
        .with_dimensions(300, 300)
        .with_title("display2".to_owned())
        .build_glium().unwrap();

    let mut canvas = Canvas::new(&display1, 20).unwrap();

    canvas.add_line([-0.5, -0.5], [0.0, 0.5]);
    canvas.add_line([ 0.0, 0.5], [0.5, -0.25]);

    loop {
        for ev in display2.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                glium::glutin::Event::Refresh => {
                    let mut target = display2.draw();
                    target.clear_color(0.0, 0.0, 1.0, 1.0);
                    canvas.draw(&mut target).unwrap();
                    target.finish().unwrap();
                },
                _ => ()
            }
        }
        thread::sleep(Duration::from_millis(100));
    }
}
